package com.example.faccigemacalcularhora;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {


    EditText txtnumerohoras;
    Button btnCalcular;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        txtnumerohoras = (EditText) findViewById(R.id.txtnumerohoras);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent inten = new Intent(MainActivity.this, calculosueldo.class);

                Bundle bundle = new Bundle();

                bundle.putString("Sueldo",txtnumerohoras .getText().toString());

                inten.putExtras(bundle);
                startActivity(inten);
            }
        });
    }


}

