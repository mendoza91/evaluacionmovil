package com.example.faccigemacalcularhora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class calculosueldo extends AppCompatActivity {

      TextView Mostrar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculosueldo);

        Mostrar=(TextView) findViewById(R.id.text);

        String bundle=this.getIntent().getExtras().getString("Sueldo");

        if(bundle.length()<=160){

            Double gramos=Double.valueOf(bundle)*3;
            Mostrar.setText(gramos.toString());
            Log.e("text","Conversion Finalizada");


        }else{
            Double sueldo2=Double.valueOf(bundle) * 3.25;
            Mostrar.setText(sueldo2.toString());
            Log.d("text","Conversion Finalizada");
        }
    }
}
